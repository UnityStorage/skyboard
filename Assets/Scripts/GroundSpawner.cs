﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundSpawner : MonoBehaviour
{
    public GameObject[] GroundBlockPrefabs;
    public GameObject StartGround;

    float blockZPos = 0;
    int blockCount = 7;
    float blockLength = 0f;

    public Transform PlayerTransf;
    List<GameObject> currentBlocks = new List<GameObject>();

    private void Start()
    {
        blockZPos = StartGround.transform.position.z;
        blockLength = StartGround.GetComponent<BoxCollider>().bounds.size.z;

        for (int i = 0; i < blockCount; i++)
        {
            SpawnBlock();
        }
    }

    private void SpawnBlock()
    {
        GameObject block = Instantiate(GroundBlockPrefabs[Random.Range(0, GroundBlockPrefabs.Length)], transform);
        
        blockZPos += blockLength;

        block.transform.position = new Vector3(0, 0, blockZPos);

        currentBlocks.Add(block);

    }
}
