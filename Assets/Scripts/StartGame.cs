﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartGame : MonoBehaviour
{
    //Пауза в начале игры
    public static bool GameIsPaused = true;

    public GameObject pauseMenuUI; //панель паузы с текстом в начале игры

    private void Start()
    {
        Pause(); //Пауза в начале игры
    }

    private void Update()
    {
        if (Input.anyKeyDown) //Нажать любую клавишу чтобы продолжить
        {
            if (GameIsPaused)
            {
                StartingGame(); // старт игры
            }
        }
    }

    private void Pause() // пауза
    {
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        GameIsPaused = true;
    }

    private void StartingGame() // старт
    {
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        GameIsPaused = false;
    }


}
