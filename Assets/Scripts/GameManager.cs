using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

	bool gameHasEnded = false;

	public float restartDelay = 3f;

	public GameObject WastedLevelUI;

	public void WastedLevel ()
	{
		WastedLevelUI.SetActive(true);
	}

	public void EndGame ()
	{
		if (gameHasEnded == false)
		{
			gameHasEnded = true;
			WastedLevel();
		}
	}

	public void Restart ()
	{
		WastedLevelUI.SetActive(false);
		SceneManager.LoadScene(0);
	}

}
