using UnityEngine;

public class PlayerCollision : MonoBehaviour {

	public PlayerMovement movement;     // ������� � PlayerMovement script

	// ������� ����� � ������ ������
	// �������� ���������� � ������������ � �������� �� "collisionInfo"
	void OnCollisionEnter (Collision collisionInfo)
	{
		// �������� �� ������� ���� "Obstacle"
		if (collisionInfo.collider.tag == "Obstacle")
		{
			movement.enabled = false;   // ��������� ������
			FindObjectOfType<GameManager>().EndGame();
		}
	}
}
