﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidSpawner : MonoBehaviour
{
    private float startDistance = 100f; //начало дистанции создание астероидов
    private float zDistance = 100f; // дистанция создании астероидов
    private float minSpread = 20f;
    private float maxSpread = 30f;

    public Transform playerTransform; //ссылка на игрока
    public Transform obstaclePrefab; // ссылка на префаг астероидов

    float zSpread;
    float lastZPos;

    void Start()
    {
        zSpread = Random.Range(minSpread, maxSpread);
        lastZPos = playerTransform.position.z + (startDistance - zSpread - zDistance);
    }

    void Update()
    {
        if (playerTransform.position.z - lastZPos >= zSpread)
        {
            float lanePos = Random.Range(-2, 5);
            lanePos = (lanePos - 1) * 1.5f;
            Instantiate(obstaclePrefab, new Vector3(lanePos, 1.5f, lastZPos + zSpread + zDistance), Quaternion.identity);

            lastZPos += zSpread;
            zSpread = Random.Range(minSpread, maxSpread);
        }
    }
}
