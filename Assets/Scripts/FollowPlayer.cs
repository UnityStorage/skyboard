﻿using UnityEngine;

public class FollowPlayer : MonoBehaviour {

	public Transform player;    // Ссылка на игрока
	public Vector3 offset;      // Смещение позиции (x, y, z)
	public Vector3 offsetTwo;
	

	private void Update ()
	{
		//Позиция камеры по отношению к игроку

		if (Input.GetKey("space"))
		{
			transform.position = player.position + offsetTwo;	
		}
		else
        {
			transform.position = player.position + offset;
		}
	}
}
