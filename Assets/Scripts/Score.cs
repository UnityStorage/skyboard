﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Score : MonoBehaviour {

    private float score = 0f; // Счет
    private float highScore = 0f; // Лучший счет
    
    public Text scoreText;
    public Text highScoreText;

    private void Awake()
    {
        if(PlayerPrefs.HasKey("SaveScore"))
        {
            highScore = PlayerPrefs.GetFloat("SaveScore");
        } 
    }

    private void Update()
    {
        // подсчет счета и вывод его в текстовый UI
        scoreText.text = "Score: " + score.ToString("0");
        score += Time.deltaTime;

        if (Input.GetKey("space")) // получение удвоенного счета если нажат клавиша "space"
        {
            score += Time.deltaTime * 1f;
        }

        HighScore();
    }

    public void HighScore() // Подсчет лучшего счета и перезапись, сохранение лучшего счета
    {
        highScoreText.text = "HighScore: " + highScore.ToString("0");

        if (score > highScore)
        {
            highScore = score;

            PlayerPrefs.SetFloat("SaveScore", highScore);
        }
    }
}
