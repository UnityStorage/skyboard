using UnityEngine;

public class PlayerMovement : MonoBehaviour 
{
	public Rigidbody rb;

	public float forwardForce = 4000f;	// ���� �������� ������ (��������)
	public float sidewaysForce = 100f;  // �������� ����������� �� ��� "�"  

	private float Z = -10f; //���� �������� �� ��� "Z"
	private float Z1 = 10f;

	void FixedUpdate ()
	{
		// Add a forward force
		rb.AddForce(0, 0, forwardForce * Time.deltaTime);

		if (Input.GetKey("d"))	// ������� ������ �� ������� "D"
		{
			// ����������� ������ + ���� �������
			rb.AddForce(sidewaysForce * Time.deltaTime, 0, 0, ForceMode.VelocityChange);

			if (Z1 == 10f)
			{
				transform.Rotate(new Vector3(0, 0, Z));
			}
			else
            {
				Z1 = 0f;
            }
		}

		if (Input.GetKey("a"))  // ������� ������ �� ������� "A"
		{
			// ����������� ����� + ���� �������
			rb.AddForce(-sidewaysForce * Time.deltaTime, 0, 0, ForceMode.VelocityChange);
			
			if(Z == -10f)
            {
				transform.Rotate(new Vector3(0, 0, Z1));
			}
			else
			{
				Z = 0f;
			}
		}

		//��������� ���� �������� ������ (��������) ��� ������� ������� "space" 
		if(Input.GetKey("space"))
        {
			forwardForce = 8000f;
        }

		else
        {
			forwardForce = 4000f;
        }
	}
}
